const router = require('express').Router();

const {useBrowser} = require('../helper/browser');
const {authWithBrowser} = require('../helper/facebook');
const {getPosts, latestPosts, getVideos} = require('../helper/facebook');
router.get('/', async (req, res) => {

    const browser = await useBrowser();

    if (!browser) {
        return res.json({error: 'browser not found'});
    }

    const data = {
        'posts': getPosts,
        'latest_posts': latestPosts,
        'videos': getVideos,
    };

    const sort = req.query.sort || req.body.sort || req.params.sort || 'posts';
    const keyword = req.query.keyword || req.body.keyword || req.params.keyword || '';
    const searchLimit = req.query.searchLimit || req.body.searchLimit || req.params.searchLimit || 1;
    if (!data[sort]) return res.json({error: 'sort not found'});
    if (!keyword) return res.json({error: 'keyword not found'});
    console.log('waiting for auth with browser and facebook');
    const page = await authWithBrowser(browser);
    console.log('auth with browser and facebook success')
    try {

        return res.send(await data[sort](browser, page, keyword, searchLimit));
    } catch (error) {
        console.log({
            stack: error.stack,
            error,
        })
        return res.json({stack: error.stack, error});
    } finally {
        await browser.close();
        console.log('finally');
        // page.close();
    }
});

module.exports = router;
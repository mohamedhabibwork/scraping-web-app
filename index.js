const express = require('express');
const expressCors = require('express-cors');
const router = require('./router');
const app = express();
const PORT = process.env.PORT || 3000;

app.use(express.json());

app.use(expressCors());

app.use(express.static('public'));

app.use('/api', router);

app.listen(PORT, () => console.log(`Example app listening on port ${PORT}!`));

const puppeteer = require('puppeteer');
/**
 * @returns {Promise<Browser>}
 */
const useBrowser = async () => {
    try {
        return await puppeteer.launch({
            headless: 'new',
            ignoreDefaultArgs: ['--disable-extensions'],
            // executablePath: process.env.PUPPETEER_EXECUTABLE_PATH || '/usr/bin/chromium-browser',
            args: [
                '--no-sandbox',
                // '--disabled-setupid-sandbox',
                // '--disable-setuid-sandbox',
                // '--disable-extensions',
                // '--ignore-certifcate-errors',
                // '--ignore-certifcate-errors-spki-list',
                // '--ignoreHTTPSErrors=true',
            ],
            // timeout: 0,
            // slowMo: 0,
            // devtools: false,
        });
    } catch (error) {
        console.log({stack: error.stack});
    }
}


module.exports = {
    useBrowser
};